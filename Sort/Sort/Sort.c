#define _CRT_SECURE_NO_WARNINGS 1

#include"Sort.h"
#include"Stack.h"

void PrintArray(int* a, int n)
{
	assert(a);
	for (int i = 0; i < n; i++)
	{
		printf("%d ", a[i]);
	}
}

void Swap(int* x, int* y)
{
	int tmp = *x;
	*x = *y;
	*y = tmp;
}

void InsertSort(int* a, int n)
{
	assert(a);

	for (int i = 0; i < n - 1; i++)
	{
		int end = i;
		int x = a[end + 1];
		while (end >= 0)
		{
			if (a[end] > x)
			{
				a[end + 1] = a[end];
				end--;
			}
			else
			{
				break;
			}
		}
		a[end + 1] = x;
	}
}

void ShellSort(int* a, int n)
{
	assert(a);
	int gap = n;
	while (gap > 1)
	{
		gap = gap / 3 + 1;
		for (int i = 0; i < n - gap; i++)
		{
			int end = i;
			int x = a[end + gap];

			while (end >= 0)
			{
				if (a[end] > x)
				{
					a[end + gap] = a[end];
					end -= gap;
				}
				else
				{
					break;
				}
			}
			a[end + gap] = x;
		}
	}

}

void SelectSort(int* a, int n)
{
	assert(a);

	int begin = 0;
	int end = n - 1;
	while (begin < end)
	{
		int imax = begin;
		int imin = begin;
		for (int i = begin; i <= end; i++)
		{
			if (a[i] > a[imax])
			{
				imax = i;
			}
			if(a[i] < a[imin])
			{
				imin = i;
			}
		}

		Swap(&a[begin], &a[imin]);
		if (imax == begin)
			imax = imin;
		Swap(&a[end], &a[imax]);
		begin++;
		end--;
	}
}

void BubbleSort(int* a, int n)
{
	assert(a);

	int end = n;
	while (end >= 0)
	{
		int flag = 0;
		for (int i = 0; i < end - 1; i++)
		{
			if (a[i] > a[i + 1])
			{
				flag = 1;
				Swap(&a[i], &a[i + 1]);
			}
				
		}
		end--;
		if (!flag)
			break;
	}
}

int GetMidIndex(int* a, int left, int right)
{
	int mid = (left + right) / 2;
	if (a[left] < a[right])
	{
		if (a[mid] < a[left])
		{
			return left;
		}
		else if (a[mid] > a[right])
		{
			return right;
		}
		else
		{
			return mid;
		}
	}

	else
	{
		if (a[mid] < a[right])
		{
			return right;
		}
		else if (a[mid] > a[left])
		{
			return left;
		}
		else
		{
			return mid;
		}
	}
}

int Partion1(int* a, int left, int right)
{
	int tmpK = GetMidIndex(a, left, right);
	if(left!=tmpK)
		Swap(&a[left], &a[tmpK]);
	int key = left;

	while (left < right)
	{
		while (left < right && a[right] >= a[key])
		{
			right--;
		}

		while (left < right && a[left] <= a[key])
		{
			left++;
		}

		Swap(&a[left], &a[right]);
	}
	Swap(&a[left], &a[key]);

	return left;

}

int Partion2(int* a, int left, int right)
{
	int tmpK = GetMidIndex(a, left, right);
	if (left != tmpK)
		Swap(&a[left], &a[tmpK]);
	int key = a[left];
	int pivot = left;
	while (left < right)
	{
		if (left < right && a[right] >= key)
		{
			right--;
		}
		a[pivot] = a[right];
		pivot = right;

		if (left < right && a[left] <= key)
		{
			left++;
		}
		a[pivot] = a[left];
		pivot = left;

	}

	a[pivot] = key;
	return pivot;
}

int Partion3(int* a, int left, int right)
{
	int tmpK = GetMidIndex(a, left, right);
	if (left != tmpK)
		Swap(&a[left], &a[tmpK]);
	int key = left;
	int prev = left;
	int cur = prev + 1;
	while (cur <= right)
	{
		if (a[cur] < a[key] && ++prev != cur)
		{
			Swap(&a[cur], &a[prev]);
		}
		
		cur++;

	}
	Swap(&a[prev], &a[key]);

	return prev;
}

void QuickSort(int* a, int left, int right)
{
	assert(a);

	if (left >= right)
		return;
	
	int key = Partion3(a, left, right);
	if (right - left + 1 < 10)
	{
		InsertSort(a + left, right - left + 1);
	}

	QuickSort(a, left, key - 1);
	QuickSort(a, key + 1, right);

	
}

void QuickSortNonR(int* a, int left, int right)
{
	assert(a);

	ST st;
	StackInit(&st);

	StackPush(&st, left);
	StackPush(&st, right);

	while (!StackEmpty(&st))
	{
		int end = StackTop(&st);
		StackPop(&st);
		int begin = StackTop(&st);
		StackPop(&st);

		int key = Partion3(a, begin, end);

		if (end > key + 1)
		{
			StackPush(&st, key + 1);
			StackPush(&st, end);
		}
		if (begin < key - 1)
		{
			StackPush(&st,begin);
			StackPush(&st, key - 1);
		}
	}

	StackDestory(&st);
}

void _MergeSort(int* a, int left, int right, int* tmp)
{
	if (left >= right)
		return;

	int mid = (left + right) / 2;
	_MergeSort(a, left, mid, tmp);
	_MergeSort(a, mid + 1, right, tmp);

	int begin1 = left;
	int end1 = mid;
	int begin2 = mid + 1;
	int end2 = right;
	int i = left;//����0
	while (begin1 <= end1 && begin2 <= end2)
	{
		
		if (a[begin1] < a[begin2])
		{
			tmp[i++] = a[begin1++];
			
		}
		else
		{
			tmp[i++] = a[begin2++];
			
		}
	}

	while (begin1 <= end1)
	{
		tmp[i++] = a[begin1++];
	}
	while (begin2 <= end2)
	{
		tmp[i++] = a[begin2++];
	}

	for (int j = left; j <= right; j++)
	{
		a[j] = tmp[j];
	}
}



void MergeSort(int* a, int n)
{
	assert(a);

	int* tmp = malloc(sizeof(int) * n);

	if (!tmp)
	{
		printf("malloc fail\n");
		exit(-1);
	}
	_MergeSort(a, 0, n - 1, tmp);

	free(tmp);
	tmp = NULL;
}

void MergeSortNonR(int* a, int n)
{
	assert(a);
	int* tmp = malloc(sizeof(int) * n);

	if (!tmp)
	{
		printf("malloc fail\n");
		exit(-1);
	}

	int gap = 1;
	while (gap < n)
	{
		for (int i = 0; i < n; i += 2 * gap)
		{
			int begin1 = i, end1 = i + gap - 1;
			int begin2 = i + gap, end2 = i + 2 * gap - 1;
			int index = i;
			if (end1 >= n || begin2 >= n)
			{
				break;
			}

			if (end2 >= n)
			{
				end2 = n - 1;
			}
			while (begin1 <= end1 && begin2 <= end2)
			{

				if (a[begin1] < a[begin2])
				{
					tmp[index++] = a[begin1++];

				}
				else
				{
					tmp[index++] = a[begin2++];

				}
			}

			while (begin1 <= end1)
			{
				tmp[index++] = a[begin1++];
			}
			while (begin2 <= end2)
			{
				tmp[index++] = a[begin2++];
			}

			for (int j = i; j <= end2; j++)
			{
				a[j] = tmp[j];
			}
		}
		gap *= 2;
	}
}

void CountSort(int* a, int n)
{
	assert(a);
	int max = INT_MIN;
	int min = INT_MAX;
	for (int i = 0; i < n; i++)
	{
		if (a[i] > max)
		{
			max = a[i];
		}
		if (a[i] < min)
		{
			min = a[i];
		}
	}
	int range = max - min + 1;
	int* count = (int*)malloc(sizeof(int) * range);
	if (!count)
	{
		printf("malloc fail\n");
		exit(-1);
	}

	memset(count, 0, sizeof(int) * range);
	for (int i = 0; i < n; i++)
	{
		count[a[i] - min]++;
	}
	int index = 0;
	for (int i = 0; i < range; i++)
	{
		while (count[i])
		{
			a[index++] = i + min;
			count[i]--;
		}
	}
	free(count);
	count = NULL;
}