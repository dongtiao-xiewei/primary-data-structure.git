#pragma once

#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<string.h>

void PrintArray(int* a, int n);

void Swap(int* x, int* y);

void InsertSort(int* a, int n);

void ShellSort(int* a, int n);

void SelectSort(int* a, int n);

void HeapSortDown(int* a, int n);

void HeapSortUp(int* a, int n);

void BubbleSort(int* a, int n);

void QuickSort(int* a, int left, int right);

void QuickSortNonR(int* a, int left, int right);

void MergeSort(int* a, int n);

void MergeSortNonR(int* a, int n);

void CountSort(int* a, int n);