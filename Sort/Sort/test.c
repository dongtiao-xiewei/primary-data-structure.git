#define _CRT_SECURE_NO_WARNINGS 1
#include"Sort.h"
#include<time.h>


void TestOP()
{
	srand(time(0));
	const int N = 1000000;
	int* a1 = (int*)malloc(sizeof(int) * N);
	int* a2 = (int*)malloc(sizeof(int) * N);
	int* a3 = (int*)malloc(sizeof(int) * N);
	int* a4 = (int*)malloc(sizeof(int) * N);
	int* a5 = (int*)malloc(sizeof(int) * N);
	int* a6 = (int*)malloc(sizeof(int) * N);
	int* a7 = (int*)malloc(sizeof(int) * N);
	int* a8 = (int*)malloc(sizeof(int) * N);
	for (int i = 0; i < N; ++i)
	{
		a1[i] = rand();
		a2[i] = a1[i];
		a3[i] = a1[i];
		a4[i] = a1[i];
		a5[i] = a1[i];
		a6[i] = a1[i];
		a7[i] = a1[i];
		a8[i] = a1[i];
	}
	int begin1 = clock();
	InsertSort(a1, N);
	int end1 = clock();
	printf("InsertSort:%d\n", end1 - begin1);
	int begin2 = clock();
	ShellSort(a2, N);
	int end2 = clock();
	printf("ShellSort:%d\n", end2 - begin2);
	int begin3 = clock();
	SelectSort(a3, N);
	int end3 = clock();
	printf("SelectSort:%d\n", end3 - begin3);
	int begin4 = clock();
	HeapSortDown(a4, N);
	int end4 = clock();
	printf("HeapSortDown:%d\n", end4 - begin4);
	int begin5 = clock();
	QuickSort(a5, 0, N - 1);
	int end5 = clock();
	printf("QuickSort:%d\n", end5 - begin5);
	int begin6 = clock();
	MergeSort(a6, N);
	int end6 = clock();
	printf("MergeSort:%d\n", end6 - begin6);
	int begin7 = clock();
	HeapSortUp(a7, N);
	int end7 = clock();
	printf("HeapSortUp:%d\n", end7 - begin7);
	int begin8 = clock();
	BubbleSort(a8, N);
	int end8 = clock();
	printf("BubbleSort:%d\n", end8 - begin8);
	
	
	
	
	
	
	
	free(a1);
	free(a2);
	free(a3);
	free(a4);
	free(a5);
	free(a6);
	free(a7);
	free(a8);
}

void TestInsertSort()
{
	int a[] = { 324,533,32,324,4,5,34,6546,8,9,97,6,54,4,44, };
	int sz = sizeof(a) / sizeof(a[0]);

	InsertSort(a, sz);

	PrintArray(a, sz);
}

void TestShellSort()
{
	int a[] = { 23,4,5,3,2,1,24,555,663,232,4,43,343,53,6,56,5,5,44,4 };
	int sz = sizeof(a) / sizeof(a[0]);

	ShellSort(a, sz);

	PrintArray(a, sz);
}

void TestSelectSort()
{
	int a[] = { 23,4,5,3,2,1,24,555,663,232,4,43,343,53,6,56,5,5,44,4 };
	
	int sz = sizeof(a) / sizeof(a[0]);

	SelectSort(a, sz);

	PrintArray(a, sz);
}

void TestHeapSort()
{
	int a[] = { 23,4,5,3,2,1,24,555,663,232,4,43,343,53,6,56,5,5,44,4 };

	int sz = sizeof(a) / sizeof(a[0]);

	//HeapSort(a, sz);
	HeapSortUp(a, sz);
	PrintArray(a, sz);
}

void TestBubbleSort()
{
	//int a[] = { 23,4,5,3,2,1,24,555,663,232,4,43,343,53,6,56,5,5,44,4 };
	int a[] = { 1,2,3,4,5,6,8,7 };
	int sz = sizeof(a) / sizeof(a[0]);

	//HeapSort(a, sz);
	BubbleSort(a, sz);
	PrintArray(a, sz);
}

void TestQuickSort()
{
	int a[] = { 6,7,4,2,1,5,0,9,8 ,12,4,54,4,56,7,8,4,32,4,6,77,777,345};
	
	int sz = sizeof(a) / sizeof(a[0]);

	//HeapSort(a, sz);
	//QuickSort(a, 0, sz - 1);
	QuickSortNonR(a, 0, sz - 1);
	PrintArray(a, sz);
}

void TestMergeSort()
{
	int a[] = { 6,8,10,4,1,5,2,7,3 };

	int sz = sizeof(a) / sizeof(a[0]);

	//HeapSort(a, sz);
	//QuickSort(a, 0, sz - 1);
	MergeSortNonR(a, sz);
	PrintArray(a, sz);
}

void TestCountSort()
{
	int a[] = { 1,2,3,2,1,4,4,2,3,4,5,3,2,2,4,1,2,3,4,5 };

	int sz = sizeof(a) / sizeof(a[0]);

	//HeapSort(a, sz);
	//QuickSort(a, 0, sz - 1);
	CountSort(a, sz);
	PrintArray(a, sz);
}

int main()
{
	//TestInsertSort();
	//TestShellSort();
	//TestSelectSort();
	//TestHeapSort();
	//TestBubbleSort();
	//TestQuickSort();
	//TestMergeSort();
	TestCountSort();
	//TestOP();
	return 0;
}